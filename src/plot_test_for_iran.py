# @name: plot_test.py
# @desc: Plots a test run
# @auth: Tim O'Brien
# @date: May 31, 2016

import os

import numpy as np

import matplotlib
matplotlib.use('SVG') # To generate images without window appearing.
                      # Use Agg for .png, or also PDF, SVG or PS.
import matplotlib.pyplot as plt

import pickle



def main():
    filedir = os.path.abspath(
                  os.path.join('..','data','ERAN_chorales'))
    filepath = os.path.join(filedir, 'cluster.txt')

    testpath = os.path.abspath(
                  os.path.join(
                      '.',
                      'runs',
                      'medium.cmn13.stanford.edu.1464722889.53.log_perp_list.txt'
                      )
                  )



    savefigname = "my_output.svg"

    notematrix = parse_file_to_matrix(filepath)

    notematrix = notematrix.T

    fig = plt.figure(figsize=(8, 12))
    plot_note_matrix(cluster1)
    #plt.show()
    plt.savefig(savefigname)

def plot_note_matrix(cluster1):
    h, w = cluster1.shape
    y = xrange(48)
    labels = ['c',r'c$\sharp$','d',r'd$\sharp$','e','f',r'f$\sharp$','g',\
              r'g$\sharp$','a',r'a$\sharp$','b']
    labels = np.tile(labels, 4)
    plt.imshow(cluster1, origin='lower', cmap='Greys', interpolation="nearest",
               aspect='auto')
    plt.xlim([0, w-0.5])
    plt.ylim([-0.5, h-0.5])
    plt.plot([-0.5, w-0.5], [11.5, 11.5], 'k')
    plt.plot([-0.5, w-0.5], [23.5, 23.5], 'k')
    plt.plot([-0.5, w-0.5], [35.5, 35.5], 'k')
    plt.yticks(y, labels)
    plt.xticks(np.arange(0,w+1,4.0)-0.5, np.arange(0,w+1,4))
    plt.xlabel('Time steps (eighth notes)')
    plt.ylabel('Notes for bass, tenor, alto, and soprano')
    plt.grid()

def parse_file_to_matrix(filepath):

    stringarray = np.genfromtxt(filepath, dtype="|S105")

    output = []

    for iCluster in xrange(stringarray.shape[0]):
        cluster = stringarray[iCluster]
        cluster = [c.strip('[').strip(']') for c in cluster.tolist()]

        newcluster = []
        for row in cluster:
            newrow = np.zeros((48))
            for i in range(48):
                newrow[i] = int(row[i])
            newcluster.append(newrow)
        cluster = np.asarray(newcluster)

        output.append(cluster)

    return np.asarray(output)


if __name__=="__main__":
    main()
