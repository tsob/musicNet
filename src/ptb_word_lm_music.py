# @name: ptb_word_lm_music.py
# @desc: Simple LSTM language model for use with the Bach chorales, adapted
#        from the TensorFlow example at
#        tensorflow/tensorflow/models/rnn/ptb/ptb_word_lm.py
# @auth: Adapted by Iran Roman / Tim O'Brien
# @date: May 14-15, 2016
#==============================================================================
# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Example / benchmark for building a PTB LSTM model.

Trains the model described in:
(Zaremba, et. al.) Recurrent Neural Network Regularization
http://arxiv.org/abs/1409.2329

There are 3 supported model configurations:
===========================================
| config | epochs | train | valid  | test
===========================================
| small  | 13     | 37.99 | 121.39 | 115.91
| medium | 39     | 48.45 |  86.16 |  82.07
| large  | 55     | 37.87 |  82.62 |  78.29
The exact results may vary depending on the random initialization.

The hyperparameters used in the model:
- init_scale - the initial scale of the weights
- learning_rate - the initial value of the learning rate
- max_grad_norm - the maximum permissible norm of the gradient
- num_layers - the number of LSTM layers
- num_steps - the number of unrolled steps of LSTM
- hidden_size - the number of LSTM units
- max_epoch - the number of epochs trained with the initial learning rate
- max_max_epoch - the total number of epochs for training
- keep_prob - the probability of keeping weights in the dropout layer
- lr_decay - the decay of the learning rate for each epoch after "max_epoch"
- batch_size - the batch size

The data required for this example is in the data/ dir of the
PTB dataset from Tomas Mikolov's webpage:

$ wget http://www.fit.vutbr.cz/~imikolov/rnnlm/simple-examples.tgz
$ tar xvf simple-examples.tgz

To run:

$ python ptb_word_lm.py --data_path=simple-examples/data/

"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import time
import os

from copy import deepcopy

import numpy as np
import tensorflow as tf

import reader

flags = tf.flags
logging = tf.logging

# Define arguments to our TF application
flags.DEFINE_string(
    "model",
    "medium",
    "A type of model. Possible options are: small, medium, large.")
flags.DEFINE_string(
    "data_path",
    None,
    "The path to correctly-formatted data.")
flags.DEFINE_string(
    "save_dir",
    None,
    "The path to a directory where you'd like to save weights.")
flags.DEFINE_boolean(
    "tensorboard",
    False,
    "Whether to write data to a serialized TensorBoard summary.")

flags.DEFINE_boolean(
    "hyperparam_search",
    False,
    "Whether to probabilistically search for the best hyperparameters.")

FLAGS = flags.FLAGS

TIMESTR = str(time.time())

class MusicModel(object):
    """The music model."""

    def add_placeholders(self):
        """Add input placeholders to the TensorFlow graph
        """

        batch_size = self.config.batch_size
        num_steps = self.config.num_steps

        with tf.variable_scope("inputs"):

            input_data = tf.placeholder(tf.int32, [batch_size, num_steps])
            targets = tf.placeholder(tf.int32, [batch_size, num_steps])

            if self.is_training and FLAGS.tensorboard:
                self.variable_summaries(input_data, 'input_data')
                self.variable_summaries(targets, 'targets')

        return input_data, targets


    def add_embedding(self):
        """Add word (chord) embedding layer.
        """
        hidden_size = self.config.hidden_size
        vocab_size = self.config.vocab_size

        #with tf.device("/cpu:0"):
        # Tim commented out the above -- should be OK on GPU with TF 0.8.0
        with tf.variable_scope("embedding"):
            embedding = tf.get_variable("embedding", [vocab_size, hidden_size])
            inputs = tf.nn.embedding_lookup(embedding, self._input_data)

            with tf.variable_scope("dropout"):
                if self.is_training and self.config.keep_prob < 1:
                    inputs = tf.nn.dropout(inputs, self.config.keep_prob)

        return inputs

    def add_loss(self, logits):
        """Add loss op
        """
        batch_size = self.config.batch_size
        num_steps = self.config.num_steps

        with tf.variable_scope("loss"):
            loss_per_example = tf.nn.seq2seq.sequence_loss_by_example(
                [logits],
                [tf.reshape(self._targets, [-1])],
                [tf.ones([batch_size * num_steps])])
            if self.is_training and FLAGS.tensorboard:
                self.variable_summaries(loss_per_example, 'loss_per_example')

            cost = tf.reduce_sum(loss_per_example) / self.config.batch_size
            if self.is_training and FLAGS.tensorboard:
                tf.scalar_summary("cost", cost)
        return cost

    def add_loss_per_timestep(self, logits):
        """Add op to get loss per timestep, e.g. in looking at test results.
           Should be very similar to the sequence_loss_by_example.
           See https://github.com/tensorflow/tensorflow/blob/master/tensorflow/python/ops/seq2seq.py#L832-875
        """
        logits = [logits]
        targets = [tf.reshape(self._targets, [-1])]
        weights = [tf.ones([self.config.batch_size * self.config.num_steps])]
        log_perp_list = []
        for logit, target, weight in zip(logits, targets, weights):
            crossent = tf.nn.sparse_softmax_cross_entropy_with_logits(
                                logit, target)
            log_perp_list.append(crossent * weight)

        return tf.pack(log_perp_list)

    def add_model(self, inputs):
        """Construct the model.
        """
        batch_size = self.config.batch_size
        hidden_size = self.config.hidden_size
        vocab_size = self.config.vocab_size
        num_layers = self.config.num_layers
        num_steps = self.config.num_steps

        # Note: see https://github.com/tensorflow/tensorflow/blob/master/tensorflow/python/ops/rnn_cell.py
        # for tf.nn.rnn_cell code.
        # Options include:
        #     BasicRNNCell, https://github.com/tensorflow/tensorflow/blob/master/tensorflow/python/ops/rnn_cell.py#L120-141
        #     GRUCell, https://github.com/tensorflow/tensorflow/blob/master/tensorflow/python/ops/rnn_cell.py#L144-173
        #     BasicLSTMCell, https://github.com/tensorflow/tensorflow/blob/master/tensorflow/python/ops/rnn_cell.py#L176-244
        #     LSTMCell, https://github.com/tensorflow/tensorflow/blob/master/tensorflow/python/ops/rnn_cell.py#L283-455
        with tf.variable_scope("rnn"):
            lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(
                                                  hidden_size, forget_bias=1.0)

            # Add dropout.
            if self.is_training and self.config.keep_prob < 1:
                lstm_cell = tf.nn.rnn_cell.DropoutWrapper(
                    lstm_cell,
                    output_keep_prob = self.config.keep_prob
                    )

            # Make this multi-layered
            cell = tf.nn.rnn_cell.MultiRNNCell([lstm_cell] * num_layers)

            self._initial_state = cell.zero_state(batch_size, tf.float32)

        from tensorflow.models.rnn import rnn
        inputs = [tf.squeeze(input_, [1]) \
                  for input_ in tf.split(1, num_steps, inputs)]
        outputs, state = rnn.rnn(
                             cell, inputs, initial_state=self._initial_state)

        output = tf.reshape(tf.concat(1, outputs), [-1, hidden_size])

        self._final_state = state

        with tf.variable_scope("softmax_prep"):
            softmax_w = tf.get_variable("softmax_w", [hidden_size, vocab_size])
            softmax_b = tf.get_variable("softmax_b", [vocab_size])
            logits = tf.matmul(output, softmax_w) + softmax_b

            if self.is_training and FLAGS.tensorboard:
                self.variable_summaries(softmax_b, 'softmax_b')
                self.variable_summaries(softmax_w, 'softmax_w')
                self.variable_summaries(logits, 'logits')

        return logits


    def add_train_op(self, cost):
        """Add the operation for training
        """
        with tf.variable_scope("training"):
            self._lr = tf.Variable(0.0, trainable=False)
            tvars = tf.trainable_variables()
            grads, _ = tf.clip_by_global_norm(tf.gradients(cost, tvars),
                                              self.config.max_grad_norm)

            # Optimization
            optimizer = tf.train.GradientDescentOptimizer(self._lr)
            train_op = optimizer.apply_gradients(zip(grads, tvars))

        return train_op


    def __init__(self, is_training, config, is_testing=False):
        """Initialize the model.
        """
        # For TensorBoard
        if FLAGS.tensorboard:
            self.train_writer = config.train_writer
            self.i = 0

        # Are we training this model?
        self.is_training = is_training

        # Config params
        self.config = config
        batch_size  = config.batch_size
        num_steps   = config.num_steps
        hidden_size = config.hidden_size
        vocab_size  = config.vocab_size
        keep_prob   = config.keep_prob

        self._input_data, self._targets = self.add_placeholders()
        inputs = self.add_embedding()
        logits = self.add_model(inputs)
        self._cost = cost = self.add_loss(logits)

        if is_training:
            self._train_op = self.add_train_op(cost)

        if is_testing:
            self.log_perp_list = self.add_loss_per_timestep(logits)

    def assign_lr(self, session, lr_value):
        """Assign a learning rate
        """
        session.run(tf.assign(self.lr, lr_value))

    def variable_summaries(self, var, name):
        """Attach a lot of summaries to a Tensor.
        Taken from https://github.com/tensorflow/tensorflow/blob/r0.8/tensorflow/examples/tutorials/mnist/mnist_with_summaries.py
        """
        with tf.name_scope('summaries'):
            float_var = tf.to_float(var)
            mean = tf.reduce_mean(float_var)
            tf.scalar_summary('mean/' + name, mean)
            with tf.name_scope('stddev'):
                stddev = tf.sqrt(tf.reduce_sum(tf.square(float_var) - mean))
            tf.scalar_summary('sttdev/' + name, stddev)
            tf.scalar_summary('max/' + name, tf.reduce_max(var))
            tf.scalar_summary('min/' + name, tf.reduce_min(var))
            tf.histogram_summary(name, float_var)

    @property
    def input_data(self):
        return self._input_data

    @property
    def targets(self):
        return self._targets

    @property
    def initial_state(self):
        return self._initial_state

    @property
    def cost(self):
        return self._cost

    @property
    def final_state(self):
        return self._final_state

    @property
    def lr(self):
        return self._lr

    @property
    def train_op(self):
        return self._train_op


class SmallConfig(object):
    """Small config."""
    init_scale = 0.1
    learning_rate = 1.0
    max_grad_norm = 5
    num_layers = 2
    num_steps = 20
    hidden_size = 200
    max_epoch = 4
    max_max_epoch = 13
    keep_prob = 1.0
    lr_decay = 0.5
    batch_size = 20
    vocab_size = 10000
    # For tensorboard
    if FLAGS.tensorboard:
        train_writer = tf.train.SummaryWriter('../tensorboard_summaries')


class MediumConfig(object):
    """Medium config."""
    init_scale = 0.05
    learning_rate = 1.0
    max_grad_norm = 5
    num_layers = 2
    num_steps = 35
    hidden_size = 400 #650
    max_epoch = 6
    max_max_epoch = 39
    keep_prob = 0.5
    lr_decay = 0.8
    batch_size = 20
    vocab_size = 10000
    # For tensorboard
    if FLAGS.tensorboard:
        train_writer = tf.train.SummaryWriter('../tensorboard_summaries')


class LargeConfig(object):
    """Large config."""
    init_scale = 0.04
    learning_rate = 1.0
    max_grad_norm = 10
    num_layers = 2
    num_steps = 35
    hidden_size = 1500
    max_epoch = 14
    max_max_epoch = 55
    keep_prob = 0.35
    lr_decay = 1 / 1.15
    batch_size = 20
    vocab_size = 10000
    # For tensorboard
    if FLAGS.tensorboard:
        train_writer = tf.train.SummaryWriter('../tensorboard_summaries')


class TestConfig(object):
    """Tiny config, for testing."""
    init_scale = 0.1
    learning_rate = 1.0
    max_grad_norm = 1
    num_layers = 1
    num_steps = 2
    hidden_size = 2
    max_epoch = 1
    max_max_epoch = 1
    keep_prob = 1.0
    lr_decay = 0.5
    batch_size = 20
    vocab_size = 10000
    # For tensorboard
    if FLAGS.tensorboard:
        train_writer = tf.train.SummaryWriter('../tensorboard_summaries')


def run_epoch(session, m, data, eval_op, verbose=False, is_testing=False):
    """
    Runs the model on the given data for one epoch.
    """
    epoch_size = ((len(data) // m.config.batch_size) - 1) // m.config.num_steps
    start_time = time.time()
    costs = 0.0
    iters = 0
    state = m.initial_state.eval()

    if is_testing and (FLAGS.save_dir is not None):
        log_perp_list = None

    for step, (x, y) in enumerate(reader.ptb_iterator(
                                   data, m.config.batch_size, m.config.num_steps)):
        if m.is_training and FLAGS.tensorboard:
            summary, cost, state, _ = session.run(
                    [m.merged, m.cost, m.final_state, eval_op],
                    { m.input_data: x,
                      m.targets: y,
                      m.initial_state: state }
                    )
            m.train_writer.add_summary(summary, m.i)
            m.i = m.i + 1
        #elif is_testing and (FLAGS.save_dir is not None):
        elif is_testing and (FLAGS.save_dir is not None):
            # For the test run, we want to fetch the loss per time step and
            # save it.
            batch_log_perp_list, cost, state, _ = session.run(
                    [m.log_perp_list, m.cost, m.final_state, eval_op],
                    { m.input_data: x,
                      m.targets: y,
                      m.initial_state: state }
                    )
            if log_perp_list is None:
                log_perp_list = batch_log_perp_list
            else:
                log_perp_list = np.vstack((log_perp_list, batch_log_perp_list))
        else:
            cost, state, _ = session.run(
                    [m.cost, m.final_state, eval_op],
                    { m.input_data: x,
                      m.targets: y,
                      m.initial_state: state }
                    )
        costs += cost
        iters += m.config.num_steps

        if verbose and step % (epoch_size // 10) == 10:
            print("%.3f perplexity: %.3f speed: %.0f wps" %
                  (step * 1.0 / epoch_size, np.exp(costs / iters),
                   iters * m.config.batch_size / (time.time() - start_time)))


    if is_testing and (FLAGS.save_dir is not None):
        save_file_path = os.path.join(
                os.path.abspath(FLAGS.save_dir),
                FLAGS.model + '.' + os.getenv('HOSTNAME') + '.' + TIMESTR
                )
        log_perp_file_path = save_file_path + '.log_perp_list.txt'
        import pickle
        print("Writing log perp list to %s" % (log_perp_file_path))
        with open(log_perp_file_path, 'ab+') as f:
            pickle.dump(log_perp_list, f)

    return np.exp(costs / iters)


def get_config():
    if FLAGS.model == "small":
        return SmallConfig()
    elif FLAGS.model == "medium":
        return MediumConfig()
    elif FLAGS.model == "large":
        return LargeConfig()
    elif FLAGS.model == "test":
        return TestConfig()
    else:
        raise ValueError("Invalid model: %s", FLAGS.model)

def get_random_config():
    """Get a randomly generated set of hyperparameters
    """
    config = TestConfig()

    config.init_scale = np.random.uniform(0.2, 1.2)
    config.learning_rate = np.power(10.0, np.random.uniform(-1, 0))
    config.max_grad_norm = np.random.uniform(3,15)
    config.num_layers = np.random.randint(1,4)
    config.num_steps = np.random.randint(8,50)
    config.hidden_size = np.random.randint(10,500)
    config.max_epoch = np.random.randint(3,30)
    config.max_max_epoch = np.random.randint(64, 256)
    config.keep_prob = np.random.uniform(0.25,0.95)
    config.lr_decay = np.random.uniform(0.75,1.0)
    config.batch_size = np.random.randint(1,32)
    config.vocab_size = 10000

    return config

def main(_):
    if not FLAGS.data_path:
        raise ValueError("Must set --data_path to PTB data directory")


    raw_data = reader.ptb_raw_data(FLAGS.data_path)
    train_data, valid_data, test_data, _, word_to_id = raw_data

    if not FLAGS.hyperparam_search:
        config = get_config()
        eval_config = get_config()
        eval_config.batch_size = 1
        eval_config.num_steps = 1

        with tf.Graph().as_default(), tf.Session() as session:
            # Random uniform initialization for everything
            initializer = tf.random_uniform_initializer(-config.init_scale,
                                                        config.init_scale)

            # Setup a model for training
            with tf.variable_scope("model", reuse=None, initializer=initializer):
                m = MusicModel(is_training=True, config=config)

            # Setup separate models for validation and testing,
            # which reuse the above model's variables.
            with tf.variable_scope("model", reuse=True, initializer=initializer):
                mvalid = MusicModel(is_training=False, config=config)
                mtest = MusicModel(is_training=False, config=eval_config,
                                   is_testing=True)

            # TensorBoard -- add graph
            if FLAGS.tensorboard:
                m.merged = tf.merge_all_summaries()
                m.train_writer.add_graph(session.graph)

            # Initialize variables!
            tf.initialize_all_variables().run()

            # Saver will save our good weights, hyperparameters, and training stats
            if FLAGS.save_dir is not None:
                saver = tf.train.Saver()
                save_file_path = os.path.join(
                        os.path.abspath(FLAGS.save_dir),
                        FLAGS.model + '.' + os.getenv('HOSTNAME') + '.' + TIMESTR
                        )
                weights_file_path = save_file_path + '.weights'
                params_file_path = save_file_path + '.hyperparams.txt'
                stats_file_path = save_file_path + '.stats.txt'
                word2id_file_path = save_file_path + '.word2id.txt'
                print("\nWeights will be stored in %s\n" % (weights_file_path) )
                print("\nHyperparams will be stored in %s\n\n" % (params_file_path) )
                with open(params_file_path, "w") as f:
                    d = config.__class__.__dict__
                    f.write(repr(d))

                # Keep track of training history
                stats = {}
                stats['train_perplexity'] = []
                stats['valid_perplexity'] = []

            # Keep track of best-performing epochs, re: validation
            best_valid_perplexity = float('inf')
            best_valid_epoch = 0

            # Train!
            for i in range(config.max_max_epoch):
                # Apply learning rate decay and set learning rate
                lr_decay = config.lr_decay ** max(i - config.max_epoch, 0.0)
                m.assign_lr(session, config.learning_rate * lr_decay)

                print("Epoch: %d Learning rate: %.3f" % \
                        (i + 1, session.run(m.lr)))

                train_perplexity = run_epoch(session, m, train_data, m.train_op,
                                             verbose=True)
                if FLAGS.save_dir is not None:
                    stats['train_perplexity'].append(train_perplexity)

                print("Epoch: %d Train Perplexity: %.3f" % \
                        (i + 1, train_perplexity))

                valid_perplexity = run_epoch(session, mvalid, valid_data,
                                             tf.no_op())
                if FLAGS.save_dir is not None:
                    stats['valid_perplexity'].append(valid_perplexity)

                print("Epoch: %d Valid Perplexity: %.3f" % \
                        (i + 1, valid_perplexity))

                if FLAGS.save_dir is not None:
                    if valid_perplexity < best_valid_perplexity:
                        best_valid_perplexity = valid_perplexity
                        best_valid_epoch = i
                        saver.save(session, weights_file_path)

            # Finally, run on the test set
            if FLAGS.save_dir is not None:
                saver.restore(session, weights_file_path) # Restore best weights
            test_perplexity = run_epoch(session, mtest, test_data, tf.no_op(),
                                        is_testing=True)
            print("Test Perplexity: %.3f" % test_perplexity)

            if FLAGS.save_dir is not None:
                stats['test_perplexity'] = test_perplexity
                import pickle
                with open(stats_file_path, 'ab+') as f:
                    pickle.dump(stats, f)
                with open(word2id_file_path, 'ab+') as f:
                    pickle.dump(word_to_id, f)


    else:
        ######################################################################
        # HYPERPARAMETER SEARCH
        ######################################################################
        FLAGS.model = 'best'

        # Keep track of best-performing epochs, re: validation
        best_valid_perplexity = float('inf')

        early_stopping = 5

        # Saver will save our good weights, hyperparameters, and training stats
        if FLAGS.save_dir is not None:
            save_file_path = os.path.join(
                    os.path.abspath(FLAGS.save_dir),
                    FLAGS.model + '.' + os.getenv('HOSTNAME') + '.' + TIMESTR
                    )
            weights_file_path = save_file_path + '.weights'
            params_file_path = save_file_path + '.hyperparams.txt'
            stats_file_path = save_file_path + '.stats.txt'
            word2id_file_path = save_file_path + '.word2id.txt'
            print("\nWeights will be stored in %s\n" % (weights_file_path) )
            print("\nHyperparams will be stored in %s\n" % (params_file_path) )
            print("\nTraining stats will be stored in %s\n" % (stats_file_path) )
            print("\nword_to_id dict will be stored in %s\n" % (word2id_file_path) )

        for search_iter in xrange(100): # Search 100 random configs
            tf.python.framework.ops.reset_default_graph() # Reset graph

            best_valid_epoch = 0
            try:
                # Random config
                config = get_random_config()
                print("Randomly created config:")
                for attr in dir(config):
                    print("\tconfig.%s = %s" % (attr, getattr(config, attr)))

                eval_config = deepcopy(config)
                eval_config.batch_size = 1
                eval_config.num_steps = 1

                best_so_far = False
                best_valid_current_perplexity = float('inf')

                with tf.Graph().as_default(), tf.Session() as session:
                    # Random uniform initialization for everything
                    initializer = tf.random_uniform_initializer(-config.init_scale,
                                                                config.init_scale)

                    # Setup a model for training
                    with tf.variable_scope("model", reuse=None, initializer=initializer):
                        m = MusicModel(is_training=True, config=config)

                    # Setup separate models for validation and testing,
                    # which reuse the above model's variables.
                    with tf.variable_scope("model", reuse=True, initializer=initializer):
                        mvalid = MusicModel(is_training=False, config=config)

                    # TensorBoard -- add graph
                    if FLAGS.tensorboard:
                        m.merged = tf.merge_all_summaries()
                        m.train_writer.add_graph(session.graph)

                    # Initialize variables!
                    tf.initialize_all_variables().run()

                    if FLAGS.save_dir is not None:
                        saver = tf.train.Saver()
                        # Keep track of training history
                        stats = {}
                        stats['train_perplexity'] = []
                        stats['valid_perplexity'] = []

                    # Train!
                    for i in range(config.max_max_epoch):
                        # Apply learning rate decay and set learning rate
                        lr_decay = config.lr_decay ** max(i - config.max_epoch, 0.0)
                        m.assign_lr(session, config.learning_rate * lr_decay)

                        print("Epoch: %d Learning rate: %.3f" % \
                                (i + 1, session.run(m.lr)))

                        train_perplexity = run_epoch(session, m, train_data, m.train_op,
                                                     verbose=False)
                        if FLAGS.save_dir is not None:
                            stats['train_perplexity'].append(train_perplexity)

                        print("Epoch: %d Train Perplexity: %.3f" % \
                                (i + 1, train_perplexity))

                        valid_perplexity = run_epoch(session, mvalid, valid_data,
                                                     tf.no_op())
                        if FLAGS.save_dir is not None:
                            stats['valid_perplexity'].append(valid_perplexity)

                        print("Epoch: %d Valid Perplexity: %.3f" % \
                                (i + 1, valid_perplexity))

                        if FLAGS.save_dir is not None:
                            if valid_perplexity < best_valid_perplexity:
                                best_valid_perplexity = valid_perplexity
                                saver.save(session, weights_file_path)
                                best_so_far = True

                        if valid_perplexity < best_valid_current_perplexity:
                            best_valid_current_perplexity = valid_perplexity
                            best_valid_epoch = i

                        # Early stopping criterion
                        if i - best_valid_epoch > early_stopping:
                            print("Hit early stopping criterion..............")
                            break

                    if (FLAGS.save_dir is not None) and (best_so_far is True):
                        import pickle
                        with open(stats_file_path, 'ab+') as f:
                            pickle.dump(stats, f)
                        with open(word2id_file_path, 'ab+') as f:
                            pickle.dump(word_to_id, f)
                        with open(params_file_path, "w") as f:
                            d = config.__class__.__dict__
                            f.write(repr(d))
            except:
                pass

        print("\nAfter %d random configs, the best validation perplexity was %f" % (
              search_iter+1, best_valid_perplexity))
        print("\nSee %s.* for saved files.\n" % (save_file_path))

if __name__ == "__main__":
    tf.app.run()
