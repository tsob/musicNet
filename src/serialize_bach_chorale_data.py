#!/usr/bin/env python
"""
@name: parse_data_tf.py
@desc: For reading in data to TF
@auth: Tim O'Brien
@date: 05/13/2016
"""

import numpy as np
import tensorflow as tf
import os
import glob

def _int64_feature(value):
    """
    Helper function to serialize ints
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    """
    Helper function to serialize bytes
    """
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def main():
    """
    Perform the data parsing and saving.
    """
    # Define where our data lives
    data_dir = os.path.abspath(os.path.join(
        '..', 'data', 'bach-chorales-20160512'
        ))
    data_filenames = [os.path.join(data_dir, os.path.basename(f)) \
                      for f in glob.glob(os.path.join(data_dir, '*.txt'))]

    num_files = len(data_filenames)

    tfrecord_filename = os.path.join( '..', 'data', \
                            'bach-chorales-20160512.tfrecords')
    writer = tf.python_io.TFRecordWriter(tfrecord_filename)

    print"=--="*20
    print "Writing {0:d} files to {1}.".format(num_files, tfrecord_filename)
    print"=--="*20

    for f in data_filenames:
        # Loop over each chorale
        data = np.genfromtxt(f, delimiter=',', dtype=np.int32)
        rows, cols = data.shape
        example = tf.train.Example(features=tf.train.Features(feature={
            'rows': _int64_feature(rows),
            'cols': _int64_feature(cols),
            'data_raw': _bytes_feature(data.tostring())}))
        writer.write(example.SerializeToString())
    writer.close()

if __name__=="__main__":
    main()
