# @name: plot_chorale.py
# @desc: Plots a chorale
# @auth: Tim O'Brien
# @date: May 30, 2016

import os

import numpy as np

import matplotlib
matplotlib.use('SVG') # To generate images without window appearing.
                      # Use Agg for .png, or also PDF, SVG or PS.
import matplotlib.pyplot as plt

def main():
    filedir = os.path.abspath(
                  os.path.join('..','data','bach-chorales-20160512'))
    filepath = os.path.join(filedir, 'chor001.txt')

    chor = np.genfromtxt(filepath, delimiter=',')
    h, w = chor.T.shape
    # print chor
    # print chor.shape

    y = xrange(48)
    labels = ['c',r'c$\sharp$','d',r'd$\sharp$','e','f',r'f$\sharp$','g',r'g$\sharp$','a',r'a$\sharp$','b']
    labels = np.tile(labels, 4)


    fig = plt.figure()
    plt.imshow(chor.T, origin='lower', cmap='Greys', interpolation="nearest")
    plt.xlim([0, w])
    plt.ylim([-0.5, h+0.5])
    plt.plot([0, w], [11.5, 11.5], 'k')
    plt.plot([0, w], [23.5, 23.5], 'k')
    plt.plot([0, w], [35.5, 35.5], 'k')
    plt.yticks(y, labels)
    fig.tight_layout()
    plt.savefig(os.path.abspath(
                    os.path.join('..', 'data', 'chor001.svg')))


if __name__ == "__main__":
    main()
