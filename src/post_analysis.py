# @name: post_analysis.py
# @desc: Analyze data already done with ptb_word_lm_music.py
# @auth: Tim O'Brien
# @date: May 29, 2016
"""
Use this file after you've saved a run of ptb_word_lm_music.py
"""

import time
import os

import numpy as np
import tensorflow as tf

import matplotlib
matplotlib.use('SVG') # To generate images without window appearing.
                      # Use Agg for .png, or also PDF, SVG or PS.
import matplotlib.pyplot as plt


#import ptb_word_lm_music as mn #mn for musicnet

flags = tf.flags

flags.DEFINE_string(
    "weights_path",
    None,
    "The path to a file of saved weights.")

flags.DEFINE_boolean(
    "embedding",
    True,
    "Whether to plot the embedding matrix via TSNE visualization.")

flags.DEFINE_boolean(
    "trainhist",
    True,
    "Whether to plot the training history per epoch (train and val sets).")

flags.DEFINE_boolean(
    "series_example",
    False,
    "Whether to plot prediction error per timestep for an example series.")

flags.DEFINE_boolean(
    "word_to_id",
    True,
    "Whether to use the saved word_to_id dictionary.")

FLAGS = flags.FLAGS

def main(_):
    """Generate some plots based on a given run of the RNN.
    """
    if not FLAGS.weights_path:
        raise ValueError("Must set --weights_path to a weights file")

    # Plot the training history per epoch
    if FLAGS.trainhist:
        stats_path = os.path.splitext(FLAGS.weights_path)[0] + '.stats.txt'
        import pickle
        with open(stats_path, 'rb') as f:
            stats = pickle.load(f)
        plot_training_history(stats)


    if FLAGS.embedding or FLAGS.series_example:
        with tf.Graph().as_default(), tf.Session() as session:

            saver = tf.train.import_meta_graph(FLAGS.weights_path + '.meta')
            saver.restore(session, FLAGS.weights_path)

            if FLAGS.embedding:
                # Get the embedding matrix
                embedding = [v for v in tf.trainable_variables() \
                             if v.name == 'model/embedding/embedding:0'][0]

                embedding = embedding.eval()
                if FLAGS.word_to_id:
                    word_to_id_path = os.path.splitext(FLAGS.weights_path)[0] \
                                      + '.word2id.txt'
                    import pickle
                    with open(word_to_id_path, 'rb') as f:
                        word_to_id = pickle.load(f)
                    plot_tsne(embedding, word_to_id=word_to_id)
                else:
                    plot_tsne(embedding)

            if FLAGS.series_example:
                pass

def annotate_chord_type(tsne_matrix, ids, name, color):
    """Helper function for plot_tsne()
    """
    for x, y in zip(
            tsne_matrix[ids, 0],
            tsne_matrix[ids, 1]):
        plt.annotate(
            name, #label,
            xy=(x, y), xytext=(20.0*x/abs(float(x)), 20.0*y/abs(float(y))),
            fontsize=10,
            textcoords='offset points', ha='right', va='bottom',
            bbox=dict(boxstyle='round,pad=0.5', fc=color, alpha=0.6),
            arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'))

def plot_tsne(full_matrix, name='embedding', word_to_id=None):
    """
    Plot a high-dimensional tensor in 2D using T-SNE.
    """
    from sklearn.manifold import TSNE
    tsne_model = TSNE(n_components=2, random_state=0)
    tsne_matrix = tsne_model.fit_transform(full_matrix)
    plt.figure()


    if word_to_id is None:
        plt.scatter(tsne_matrix[:, 0], tsne_matrix[:, 1])
    else:
        plt.scatter(
                tsne_matrix[:len(word_to_id), 0],
                tsne_matrix[:len(word_to_id), 1]
                )

        cmaj_ids, gmaj_ids, g7_ids = parse_labels(word_to_id)

        annotate_chord_type(tsne_matrix, cmaj_ids, 'C maj', 'green')
        annotate_chord_type(tsne_matrix, gmaj_ids, 'G maj', 'yellow')
        annotate_chord_type(tsne_matrix, g7_ids, 'G7', 'gold')

    if name is not None:
        figname = os.path.splitext(FLAGS.weights_path)[0] + '.' + name + '.tsne.svg'
    else:
        figname = os.path.splitext(FLAGS.weights_path)[0] + '.tsne.svg'

    plt.savefig(figname)
    print("\nSaved figure at %s\n" % (figname))

def plot_training_history(stats):
    """Plot the training history
    """
    plt.figure()
    plt.plot(stats['train_perplexity'], label="Training perplexity")
    plt.plot(stats['valid_perplexity'], label="Validation perplexity")
    plt.grid()
    plt.xlabel('Epoch')
    plt.ylabel('Perplexity')
    plt.title('Training history')
    plt.legend()

    figname = os.path.splitext(FLAGS.weights_path)[0] + '.training_history.svg'
    plt.savefig(figname)
    print("\nSaved figure at %s\n" % (figname))

def parse_labels(word_to_id):
    c   =      '100000000000'
    csh = dfl ='010000000000'
    d   =      '001000000000'
    dsh = efl ='000100000000'
    e   =      '000010000000'
    f   =      '000001000000'
    fsh = gfl ='000000100000'
    g   =      '000000010000'
    gsh = afl ='000000001000'
    a   =      '000000000100'
    ash = bfl ='000000000010'
    b   =      '000000000001'

    # All c major chords in root inversion
    cmaj=[]
    cmaj.append('[' + c + e + g + c + ']')
    cmaj.append('[' + c + e + c + g + ']')
    cmaj.append('[' + c + c + e + g + ']')
    cmaj.append('[' + c + c + g + e + ']')
    cmaj.append('[' + c + g + e + c + ']')
    cmaj.append('[' + c + g + c + e + ']')

    cmaj_ids = []
    for chord in cmaj:
        newid = [word_to_id[w] for w in word_to_id if w==chord]
        if newid is not None:
            cmaj_ids.append(newid)
    cmaj_ids = [item for sublist in cmaj_ids for item in sublist]

    # All g major chords in root inversion
    gmaj=[]
    gmaj.append('[' + g + b + d + g + ']')
    gmaj.append('[' + g + b + g + d + ']')
    gmaj.append('[' + g + g + b + d + ']')
    gmaj.append('[' + g + g + d + b + ']')
    gmaj.append('[' + g + d + b + g + ']')
    gmaj.append('[' + g + d + g + b + ']')

    gmaj_ids = []
    for chord in gmaj:
        newid = [word_to_id[w] for w in word_to_id if w==chord]
        if newid is not None:
            gmaj_ids.append(newid)
    gmaj_ids = [item for sublist in gmaj_ids for item in sublist]

    # All g dominant 7 chords in root inversion
    g7=[]
    g7.append('[' + g + b + d + f + ']')
    g7.append('[' + g + b + f + d + ']')
    g7.append('[' + g + d + b + f + ']')
    g7.append('[' + g + d + f + d + ']')
    g7.append('[' + g + f + b + d + ']')
    g7.append('[' + g + f + d + b + ']')
    # omit 5th
    g7.append('[' + g + b + g + f + ']')
    g7.append('[' + g + b + f + g + ']')
    g7.append('[' + g + g + f + b + ']')
    g7.append('[' + g + g + b + f + ']')
    g7.append('[' + g + f + g + b + ']')
    g7.append('[' + g + f + b + g + ']')
    # omit 3rd
    g7.append('[' + g + d + g + f + ']')
    g7.append('[' + g + d + f + g + ']')
    g7.append('[' + g + g + f + d + ']')
    g7.append('[' + g + g + d + f + ']')
    g7.append('[' + g + f + g + d + ']')
    g7.append('[' + g + f + d + g + ']')


    g7_ids = []
    for chord in g7:
        newid = [word_to_id[w] for w in word_to_id if w==chord]
        if newid is not []:
            g7_ids.append(newid)
    g7_ids = [item for sublist in g7_ids for item in sublist]

    return cmaj_ids, gmaj_ids, g7_ids



if __name__ == "__main__":
    tf.app.run()

