# @name: plot_test.py
# @desc: Plots a test run
# @auth: Tim O'Brien
# @date: May 31, 2016

import os

import numpy as np

import matplotlib
matplotlib.use('SVG') # To generate images without window appearing.
                      # Use Agg for .png, or also PDF, SVG or PS.
import matplotlib.pyplot as plt

import pickle



def main():
    filedir = os.path.abspath(
                  os.path.join('..','data','ERAN_chorales'))
    filepath = os.path.join(filedir, 'cluster.txt')

    testpath = os.path.abspath(
                  os.path.join(
                      '.',
                      'runs',
                      'medium.cmn13.stanford.edu.1464722889.53.log_perp_list.txt'
                      )
                  )
    generate_a_test_plot(filepath, 0, testpath, 0, 27, 'cluster1.svg', 'Cluster 1')
    generate_a_test_plot(filepath, 1, testpath, 27, 54, 'cluster2.svg', 'Cluster 2')

    filepath = os.path.join(filedir, 'control.txt')

    generate_a_test_plot(filepath, 0, testpath, 54, 81,  'control1.svg', 'Control 1')
    generate_a_test_plot(filepath, 1, testpath, 81, 108, 'control2.svg', 'Control 2')

    generate_a_test_plot(filepath, 2, testpath, 108, 135, 'control3.svg', 'Control 3')
    generate_a_test_plot(filepath, 3, testpath, 135, 162, 'control4.svg', 'Control 4')

    generate_a_test_plot(filepath, 4, testpath, 162, 189, 'control5.svg', 'Control 5')
    generate_a_test_plot(filepath, 5, testpath, 189, 216, 'control6.svg', 'Control 6')

    generate_a_test_plot(filepath, 6, testpath, 216, 243, 'control7.svg', 'Control 7')
    generate_a_test_plot(filepath, 7, testpath, 243, 270, 'control8.svg', 'Control 8')

    generate_a_test_plot(filepath, 8, testpath, 270, 297, 'control9.svg', 'Control 9')
    generate_a_test_plot(filepath, 9, testpath, 297, 324, 'control10.svg', 'Control 10')

    generate_a_test_plot(filepath, 8, testpath, 324, 351, 'control11.svg', 'Control 11')
    generate_a_test_plot(filepath, 9, testpath, 351, 378, 'control12.svg', 'Control 12')

    #filedir = os.path.abspath(
                  #os.path.join('..','data','chor_data_augmented'))
    #filepath = os.path.join(filedir, 'ptb.test.txt.original')

    #generate_a_test_plot(filepath, 0, testpath, 378, 598, 'chortest.svg', 'Bach chorale')

def generate_a_test_plot(filepath, fileidx, testpath, startidx, endidx, savefigname, titlestr):
    clusters = parse_file_to_matrix(filepath)

    cluster1 = clusters[fileidx].T

    siglen = endidx - startidx

    with open(testpath, 'rb') as f:
        log_perp_list = pickle.load(f)

    fig = plt.figure(figsize=(8, 12))
    plt.subplot2grid((4, 1), (0, 0))

    plt.plot(np.arange(siglen)+1, log_perp_list[startidx:endidx])
    plt.grid()
    plt.xlim([0, siglen+2])
    plt.ylim([0, 16])
    plt.ylabel("Log-perplexity per time step")
    plt.xticks(np.arange(0,siglen+2,4.0))
    plt.title(titlestr)

    plt.subplot2grid((4, 1), (1, 0), rowspan=3)
    plot_note_matrix(cluster1)
    #plt.show()
    plt.savefig(savefigname)

def plot_note_matrix(cluster1):
    h, w = cluster1.shape
    y = xrange(48)
    labels = ['c',r'c$\sharp$','d',r'd$\sharp$','e','f',r'f$\sharp$','g',\
              r'g$\sharp$','a',r'a$\sharp$','b']
    labels = np.tile(labels, 4)
    plt.imshow(cluster1, origin='lower', cmap='Greys', interpolation="nearest",
               aspect='auto')
    plt.xlim([0, w-0.5])
    plt.ylim([-0.5, h-0.5])
    plt.plot([-0.5, w-0.5], [11.5, 11.5], 'k')
    plt.plot([-0.5, w-0.5], [23.5, 23.5], 'k')
    plt.plot([-0.5, w-0.5], [35.5, 35.5], 'k')
    plt.yticks(y, labels)
    plt.xticks(np.arange(0,w+1,4.0)-0.5, np.arange(0,w+1,4))
    plt.xlabel('Time steps (eighth notes)')
    plt.ylabel('Notes for bass, tenor, alto, and soprano')
    plt.grid()

def parse_file_to_matrix(filepath):

    stringarray = np.genfromtxt(filepath, dtype="|S105")

    output = []

    for iCluster in xrange(stringarray.shape[0]):
        cluster = stringarray[iCluster]
        cluster = [c.strip('[').strip(']') for c in cluster.tolist()]

        newcluster = []
        for row in cluster:
            newrow = np.zeros((48))
            for i in range(48):
                newrow[i] = int(row[i])
            newcluster.append(newrow)
        cluster = np.asarray(newcluster)

        output.append(cluster)

    return np.asarray(output)


if __name__=="__main__":
    main()
