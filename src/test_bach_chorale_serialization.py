#!/usr/bin/env python
"""
@name: parse_data_tf.py
@desc: For reading in data to TF
@auth: Tim O'Brien
@date: 05/13/2016
"""

import numpy as np
import tensorflow as tf
import os

def main():
    """
    Test the data parsing and saving.
    """
    # Define where our data lives
    filename = os.path.abspath(os.path.join(
        '..', 'data', 'bach-chorales-20160512.tfrecords'
        ))

    filename_queue = tf.train.string_input_producer([filename], num_epochs=None)

    reader = tf.TFRecordReader()

    key, serialized_example = reader.read(filename_queue)

    features = tf.parse_single_example(
            serialized_example,
            features = {
                'rows': tf.FixedLenFeature([], tf.int64),
                'cols': tf.FixedLenFeature([], tf.int64),
                'data_raw': tf.FixedLenFeature([], tf.string)
                }
            )
    data = tf.decode_raw(features['data_raw'], tf.int32)
    data.set_shape([features['rows'], features['cols']])

    with tf.Session():
        print data.eval()


if __name__=="__main__":
    main()
