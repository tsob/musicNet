\documentclass{article} % For LaTeX2e
\usepackage{nips13submit_e,times}
\usepackage{hyperref}
\usepackage{url}
\usepackage{graphicx}
\usepackage{fancyvrb}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{wrapfig}
\usepackage{subcaption}
%\documentstyle[nips13submit_09,times,art10]{article} % For LaTeX 2.09


\title{A Recurrent Neural Network for\\
	Musical Structure Processing and Expectation}

\author{
Tim~O'Brien\\
CCRMA\\
Stanford University\\
Stanford, CA 94305 \\
\texttt{tsobrien@stanford.edu} \\
\And
Ir\'{a}n~Rom\'{a}n\\
CCRMA\\
Stanford University\\
Stanford, CA 94305 \\
\texttt{iran@stanford.edu} \\
}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle

\begin{abstract}
Research in cognitive neuroscience has identified neural activity correlated with subjects hearing an unexpected event in a musical sequence.
However, there is a lack of models attempting to explain how these computations are carried out in the human brain.
Using an augmented data set consisting of music from the western tradition (originally 371 Bach chorales), we trained a Long Short-Term Memory (LSTM) and two Recurrent Neural Network (RNN) architectures to ask: will a neural network show a larger perplexity when presented with an unexpected event in a musical sequence?
Could this higher perplexity highlight similar computations carried out by the human brain? Our results indicate that our methods to train different neural network architectures with a corpus of music were effective. Perplexity values for the LSTM architecture reached low values in the order of ~50. Visualization of the TSNE-reduced chord embedding matrix showed separation toward the edges common chords in our vocabulary compared to less common ones. Moreover, perplexity for an unexpected chord in an otherwise well-formed chord sequence was larger compared to an expected chord, but not as large as we originally hypothesized. Finally, out of all the architectures we trained, the clockwork RNN was the one less prone to overfitting the data, and the one that generated the most sensical chord progressions after training. Our results show a promising path moving toward the development of a neural network model of musical structure processing and expectation.\footnote{Accompanying code is available at \url{https://cm-gitlab.stanford.edu/tsob/musicNet/}}
\end{abstract}

\section{Introduction}
\label{sec:introduction}

Similar to language, music is a universal element of the human condition.
Because of its organized nature, all humans exposed to music implicitly learn the hierarchical dependencies connecting events in music, even if they do not explicitly receive formal musical training \cite{bigand2006we}.
Research in neuroscience shows that all humans are able to identify events that violate common structures in music. 
Recently, Rom\'{a}n and Fujioka found evidence showing that humans with different levels of musical training group musical sequences in structures that follow or violate local and global dependencies \cite{Roman2016musicsyntactic}.
However, there is a lack of models explaining the underlying computations taking place in the brain when humans listen to sound events unfolding in a musical sequence.
Our goal is to develop such a model explaining how humans analyze unexpected events in music which elicit surprise. We use the term \textit{surprise} to encompass the perceptions of subverted expectation, a sense of musical novelty, or the notion of a ``wrong" or out-of-place note or chord.

A recurrent neural network (RNN) can carry out such a task. RNNs can develop expectations in sequences, relying on information about recent events and hence the structural dependencies in musical sequences. Such an approach is similar to language models in the domain of natural language processing. After training, such a model could recognize unexpected events in a musical sequence in a manner similar to humans. Additionally, the network can generate musical sequences after training, demonstrating the abstractions it has learned from the data on which it was trained.

\section{Background}
\label{sec:background}

Cognitive neuroscience provides evidence about how humans process sequences of events through time \cite{lalitte2006music}, how sensory and cognitive priming lead to the construction of mental representations of musical events \cite{bigand2003sensory}, and how the life background of an individual affects expectation when listening to a sequence of musical events \cite{schubert2006effect}. Inspired by this evidence, Berger and Gang \cite{Berger2010, Gang1996, Gang1998, Gang1999} hypothesized and developed a computational model consisting of simple recurrent networks, among others, able to parse musical sequences of events and develop expectations after training.

More recently, Rabovsky \cite{Rabovsky2014} revisited the work by McClelland \textit{et al.} \cite{Mcclelland1989} on computational modelling of language cognition. Rabovsky found that McClelland's model, similarly to other models by Frank \cite{Frank2015} for semantic processing, feature large activations in the second hidden layer when presented with semantic violations. Such semantic violations are also known to elicit specific patterns of brain activity in humans, as measured by EEG \cite{Rabovsky2014}.

Our interests are more aligned with syntax (structure) processing than semantics and thus we believe that Rabovsky's architecture will not serve our purposes. Instead an RNN, similar to and building upon the work by Berger and Gang, will be our architecture of choice. Thus, considering the structural similarities between music and language, we built a language model trained on musical structures that shows larger perplexity when stimulated with unexpected events that violate the syntactic rules underlying musical flow.

% TODO for Tim: more explicit characterization of Berger's previous work



\section{Approach}
\label{sec:approach}
The methods described here allowed us to train an RNN on a corpus of music that consists of sequences of chords. Our data is analogous in structure to language, since chords are discrete events, similar to words, and sequences of chords form musical phrases, similar to how words in language form sentences. Thus, the dependencies between simultaneous chords in our data-set will be captured by the weights in the hidden modules of our Neural Network architectures.

\subsection{Data Set}
\label{sec:data}
We obtained a data-set consisting of all four-voice chorales by Johann Sebastian Bach in the HumDrum digital format. This corpus of music contains musical features that are common in western music, and should capture the musical abstractions that individuals in the western world are exposed to since an early age. Bach was an iconic composer from the 18th century. He wrote pieces that set the stylistic foundations for composers of classical music. Among Bach's oeuvre, his chorales (371 pieces in four voices: bass, tenor, alto, and soprano) are an influential body of music sharing multiple features with western music from other composers and other eras. Bach chorales consist of four different but simultaneously sounding voices. These voices coexist in separate pitch ranges (bass, tenor, alto, soprano). For en example of this musical texture, see the fragment in figure \ref{fig:bach_chorale_score_snippet}. 

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.3\textwidth]{img/bach_chorale_score_snippet.png}
\end{center}
\caption{The beginning of a Bach chorale.}\label{fig:bach_chorale_score_snippet}
\end{figure}

\subsection{Data Preparation}
\label{sec:dataprep}
In western music there are 12 different pitch classes within an octave. We parsed Bach chorales by concatenating four octaves (one for each voice) in a 48-dimensional vector (12 dimensions for each of the voices). Thus, a 48-dimensional row vector corresponds to the sonority produced by the four simultaneously sounding voices at a point in time. Since rhythm in music can make these chords sound repeatedly for an extended period of time, repeated row vectors indicate a chord lasting longer than the shortest unit of musical subdivision we chose, which turned out to be an eighth note. Fig.~\ref{fig:bach_chorale_score_parsed} shows our parsed representation of the fragment from Fig.~\ref{fig:bach_chorale_score_snippet}. 

Our original data set only contained 370 chorales, all in the same key of C major. In order to expand our data set, we used a standard method from music theory to augment our musical material called ``transposition." Transposing a piece changes the reference pitch that defines the ``tonic" of the piece. After carrying out data augmentation, all twelve pitches in the set served as tonic for each of our chorales. This data augmentation procedure expanded our data set by a factor of 11.

Thus, in both our original and augmented data-sets, a ``word" is each of the time-step vectors (48 dimensional containing all voices sounding simultaneously), and a ``sentence" is a sequence of these row vectors unfolding over time to form an entire chorale.

% PARSED DATA EXAMPLE
\renewcommand{\theFancyVerbLine}{%
 \textcolor{blue}{\tiny
  t=\arabic{FancyVerbLine}}}
\begin{figure}[htb]
\begin{center}
\begin{LVerbatim}[label=Bach chorale example, numbers=left, numbersep=2pt, fontsize=\tiny, fontfamily=courier, frame=single, commandchars=\\\{\}]
\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0, 0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}}, 0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}}, 0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0
\end{LVerbatim}
\end{center}
\caption{The same Bach chorale snippet as in Fig.~\ref{fig:bach_chorale_score_snippet}, encoded as concatenated length-12 row vectors for each time-step, corresponding to the duration of an eighth note. The groups of 12 numbers represent, from left to right, the bass, tenor, alto, and soprano.}\label{fig:bach_chorale_score_parsed}
\end{figure}


\subsection{Neural Network Architectures}
\label{sec:architectures}

Our experiments focused on two flavors of feed-forward recurrent neural networks, the LSTM (see \S \ref{sec:lstm}) and the Clockwork RNN (see \S \ref{sec:clockwork}). In both architectures, we take each chord (\textit{i.e.} the ``word" as described in \S\ref{sec:dataprep}) and learn an embedding matrix such that each unique chord is represented as a vector in our input space. 


\subsubsection{LSTM}
\label{sec:lstm}

We use a multi-layer LSTM architecture. For practical computational reasons, our experiments consisted of versions with two LSTM layers, as is depicted in Figure~\ref{fig:lstm}. For each LSTM layer, we may characterize the computation by the following equations. Equations \ref{eq:lstm:input}-\ref{eq:lstm:newmemory} define what are known as the input gate $i^{(t)}$, forget gate $f^{(t)}$, output gate $o^{(t)}$, and new memory cell $\~{c}^{(t)}$. 
\begin{align}
\label{eq:lstm:input}
    i^{(t)} &= \sigma \left( W^{(i)} x^{(t)} + U^{(i)} h^{(t-1)} \right) \\
\label{eq:lstm:forget}
    f^{(t)} &= \sigma \left( W^{(f)} x^{(t)} + U^{(f)} h^{(t-1)} \right) \\
\label{eq:lstm:output}
    o^{(t)} &= \sigma \left( W^{(o)} x^{(t)} + U^{(o)} h^{(t-1)} \right) \\
\label{eq:lstm:newmemory}
    \~{c}^{(t)} &= \tanh \left( W^{(c)} x^{(t)} + U^{(c)} h^{(t-1)} \right)
\end{align}
Note that $\sigma$ denotes the sigmoid function, $\sigma(z) = \left(1+e^{-z}\right)^{-1}$. The superscripts $^{(t)}$ denote the index of the current time step. Using the above equations, we can compute our final memory cell,
\begin{equation}
\label{eq:lstm:finalmemory}
    c^{(t)} = f^{(t)} \circ \~{c}^{(t-1)} + i^{(t)} \circ \~{c}^{(t)} \;,
\end{equation}
where $\circ$ denotes the Hadamard product. Finally, our hidden state is
\begin{equation}
\label{eq:lstm:hiddenstate}
    h^{(t)} = o^{(t)} \circ \tanh \left( c^{(t)} \right) \; .
\end{equation}
The new memory $\tilde{c}^{(t)}$ and hidden state $h^{(t)}$ are passed along for use in computing the subsequent time step ($t+1$) values, and the hidden state $h^{(t)}$ is passed forward to serve as input to the next layer in the current time step $t$ (\textit{i.e.} the next LSTM layer or the output layer). 

Thus, there are eight learned matrices in each LSTM layer: four each of the $W^{(\cdot)}$ and $U^{(\cdot)}$ matrices in Equations~\ref{eq:lstm:input}-\ref{eq:lstm:newmemory}. For our implementation, we initialize these matrices with a uniform random zero-mean distribution. The size of the distribution (\textit{i.e.} the maximum absolute value of any element) is a tunable hyperparameter.

%\begin{equation}
%    c^{(t)} = f^{(t)} \circ \left( W^{(c)} x^{(t)} + U^{(c)} h^{(t-1)} \right)
%\end{equation}


The system includes gradient norm clipping in order to prevent exploding gradients. Gradient norm clipping was first introduced by Pascanu, Mikolov, and Bengio \cite{gradnormclipping}, and prevents exploding gradients (and thus exploding stochastic gradient descent training) by clipping gradients whose norms exceed a certain threshold. This threshold in our case is a tunable hyperparameter.


\begin{figure}[htbp]
\begin{minipage}[b]{0.49\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth, trim={0 2.5cm 0 0}, clip]{img/lstm2.pdf}
    \end{center}
    \caption{The two-tiered LSTM architecture.\footnote{Note that the LSTM layers in the diagram were taken from \href{https://github.com/colah}{Christopher Olah's} \href{https://colah.github.io/posts/2015-08-Understanding-LSTMs/}{blog post}, \url{https://colah.github.io/posts/2015-08-Understanding-LSTMs/}}}\label{fig:lstm}
\end{minipage}\hfill\begin{minipage}[b]{0.49\textwidth}
    \begin{center}
    \includegraphics[width=\textwidth]{img/cwRNN.png}
    \caption{The clockwork RNN has $g$ hidden layers with different time steps $T_i$. In this figure faster time steps are on the left ($T_1, T_2$) and slower ones are on the right ($T_g$). Notice that hidden layers with slower time steps are connected to faster time steps, but not vice-versa.}\label{fig:cwRNN}
    \end{center}
\end{minipage}
\end{figure}


\subsubsection{Clockwork RNN}
\label{sec:clockwork}
Another neural network architecture we trained was the clockwork RNN (cwRNN) \cite{Koutnik2014}. The cwRNN is in essence a standard RNN, but has hidden layers whose weight matrices get updated at different time steps. A diagram of this model is depicted in figure \ref{fig:cwRNN}. The different time steps for each hidden layer allow this architecture to learn both long-term and short-term dependencies in the sequential data that it is trained on. Moreover, in the cwRNN a given hidden layer with time step $T_i$ receives information from another hidden layer with time step $T_g$ if $T_i < T_g$. This allows faster hidden layers to capture information about the large-scale dependencies in the data that slower hidden layers learn. 

The computation of the state in this cwRNN at every time-step is given by the expression:
$$y_{H}^{(t)}=f_H(W_H\cdot y^{(t-1)} + W_I\cdot x^{(t)})$$ $$y_O^{(t)}=f_O(W_O\cdot y_H^{(t-1)})$$ where the subindices $I$, $H$, and $O$, indicate elements in the input, hidden, and output layers. $x^(t)$ is the input of the network, and $f$ describes activation functions for the hidden and output layers of the network. However, only the rows and columns in $W_I$ and $W_H$ for hidden layers that suffice $t \ MOD 
\ T_i = 0$ get updated at time step $t$. Thus, the matrices $W_I$ and $W_H$ have g rows, but matrix $W_H$ is a block-upper triangular matrix with 

\begin{equation}
  W_H=\left\{
  \begin{array}{@{}ll@{}}
    W_{Hi} & \text{for}\ (t \ MOD \ T_i) = 0\\
    0 & \text{otherwise}
  \end{array}\right.
\end{equation} 

The implementation of our clockwork RNN, is a modification of the text-specific cwRNN model in the \texttt{theanets} package.\footnote{\texttt{theanets} is available at thefollowing repository: \url{https://github.com/lmjohns3/theanets}} The \texttt{theanets} implementation of this cwRNN is an implementation following the description in the original paper \cite{Koutnik2014}. Our implementation differs mainly from the original in that the text was originally parsed so that each letter corresponds to a time-step, while our model requires text to be treated on a word-by-word basis, where each sequential chord in our data corresponds to a time step.

\section{Experiments and Results}
\label{sec:experiment}

To test our architectures, we carried out different experiments with the LSTM and the clockwork RNN architectures previously described. All code is available at the following git repository: \url{https://cm-gitlab.stanford.edu/tsob/musicNet/}.


% This section begins with what kind of experiments you're doing, what kind of dataset(s) you're using, and what is the way you measure or evaluate your results. It then shows in details the results of your experiments. By details, we mean both quantitative evaluations (show numbers, figures, tables, etc) as well as qualitative results (show images, example results, etc).

\subsection{LSTM}

All LSTM experiments were run using TensorFlow 0.8.0 on a machine running Fedora Linux (Planet CCRMA\footnote{\url{http://ccrma.stanford.edu/planetccrma/software/}}), and utilizing an NVIDIA GeForce GTX 750 (1.2935 GHz) using the cuDNN 4 and CUDA 7.5.18 libraries.

Our best-performing model was trained on the aforementioned 370 four-part Bach chorales. The training set consisted of 70\% of this corpus, or 259 chorales; the validation set consisted of another 10\%, or 37 chorales; and the test set consisted of the final 20\%, or 74 chorales. With data augmentation via transposition of each chorale to each of the twelve possible keys, we have 3108, 444, and 888 chorales for the training, validation, and test sets, respectively.

\begin{wrapfigure}{r}{0.42\textwidth}
\begin{center}
\includegraphics[width=0.41\textwidth, trim={0 0.4cm 0 0.8cm}, clip]{img/lstm_training_history.pdf}
\end{center}
\caption{LSTM training history.}\label{fig:lstm-training-history}
\end{wrapfigure}

The initial scale for our trainable tensors was $0.05$, which means every element was randomly assigned a value between $-0.05$ and $0.05$. The size of our chord embedding vectors and our hidden states was $400$, and the vocabulary size was $10000$.

The learning rate, using vanilla minibatch stochastic gradient descent, was $1.0$, which decayed by a factor of $0.8$ after epoch $6$ until the final $39$th epoch. Batches of $20$ were used. The gradient norm threshold, for clipping, was $5$. During training, we applied dropout at a probability of $50\%$. The training history, plotted in Fig.~\ref{fig:lstm-training-history}, shows an orderly optimization trajectory.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.8\textwidth,trim={0 1cm 0 1cm},clip]{img/lstm_embedding_tsne.pdf}
\end{center}
\caption{A t-SNE visualization of LSTM chord embeddings, with several chord types labeled.}\label{fig:lstm-embedding}
\end{figure}

We plot a t-SNE representation \cite{tsne} of the chord embeddings in Fig.~\ref{fig:lstm-embedding}. All except one of the annotated chord types are triads, and we chose to highlight the most common four-part voicings -- namely, those in which the bass note is the root, the root is repeated in an upper voice, and no chord notes are omitted. The dominant G7 chords, on the other hand, include all voicings with a G in the bass, but allow for the omission of the 3rd or 5th in order to double the root in an upper voice and retain the 7th.

For simplicity, we present the embedding matrix from a system trained on non-augmented chorale data, all of which were normalized to have a tonic of C. Thus, for example, the green C majors may tend to represent the major I chord of a chorale, though of course modulations within a chorale were not altered, so the function of C major may be different in a modulated context.

What strikes us as interesting is that the annotated chords cluster at the edges, though different voicings of the same chord may appear far apart. Voicings of C major and G major or G dominant 7 sometimes appear in close proximity. This was at first surprising, given that the tonic and dominant serve quite distinctly different harmonic functions; on the other hand, tonic and dominant chords are highly likely to appear next to each other in sequence, for example at cadences or to establish key.

\begin{figure}[htbp]
    \centering
    \begin{subfigure}[b]{0.32\textwidth}
        \includegraphics[width=\textwidth]{img/cluster1.pdf}
        \caption{A very unexpected cacophony interrupts a normal progression.}
        \label{fig:cluster}
    \end{subfigure}~\begin{subfigure}[b]{0.32\textwidth}
        \includegraphics[width=\textwidth]{img/control11.pdf}
        \caption{A perfectly normal harmonic progression.}
        \label{fig:control11}
    \end{subfigure}~\begin{subfigure}[b]{0.32\textwidth}
        \includegraphics[width=\textwidth]{img/control12.pdf}
        \caption{A normal progression with an out-of-key penultimate chord.}
        \label{fig:control12}
    \end{subfigure}
    \caption{Perplexity per time step (top) for three input sequences (bottom, represented as stacked piano rolls for each of the four voices).}\label{fig:lstm-examples}
\end{figure}

We present the responses of our trained model to three different test signals in Fig.~\ref{fig:lstm-examples}. Fig.~\ref{fig:cluster} shows the perplexity per eighth-note time step (top signal) for a progression that unfolds normally until an abrupt cacophony of every note at every subsequent time step. This is akin to resting one's arm across all the keys of a piano. As expected, we see higher perplexity in that region of the progression. Interestingly, the perplexity subsides quickly, spikes back up, and then subsides more slowly. We conjecture that this results from the fact that, although this is indeed a surprising chord to hear, the fact the it repeats every time step is a similar gesture to other, more sensical, chord progressions with drawn-out chords. However, we would like to see a much larger spike in perplexity, corresponding to the jarringly surprising nature of the musical event represented.

Fig.~\ref{fig:control11} shows a progression which follows all rules of tonal harmony and counterpoint. Thus, we would expect to see minimal perplexity. However, there are some spikes in perplexity similar in magnitude to the previous example. We may note that the third chord is an A minor, serving as a deceptive cadence (since it comes after C major and then the G dominant 7). This is not novel and certainly occurs in the model's training set, but as the name implies, the deceptive cadence continues to subvert expectation and surprise human listeners even though they have heard deceptive cadences many times before. As such, a higher level of model perplexity for that instant is somewhat desirable and ``correct."

Finally, Fig.~\ref{fig:control12} shows a perfectly fine harmonic progression except for the penultimate chord. This chord, known as a Neapolitan, was seldom used by Bach in his chorales. Thus, we would expect a higher level or perplexity around this event, and indeed we see it. 

\subsection{cwRNN}
Using a subset of the augmented data (4000 chords total, separated into 3000 for training and 1000 for validation), we trained three different models: an RNN, an LSTM, and a clockwork RNN to assess their performance on our dataset. After training, all models were able to generate chord progressions, which varied in structural features. 

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.5\textwidth]{img/cwRNN_experiment.png}
\end{center}
\caption{The top panel shows the loss function for the training set across epochs. The bottom panel shows the loss function for the validation set. The clockwork RNN is the one that was less prone to over-fitting the subset of data it was trained with.}\label{fig:cwRNN_experiment}
\end{figure}

All models were able to over-fit the data (see Figure \ref{fig:cwRNN_experiment}), but the clockwork RNN was the one with a lesser tendency to do so, as seen in the loss functions for the validation set. This is consistent with previous findings associated with the clockwork RNN architecture when trained on sequences of events. The clockwork RNN architecture is designed to see beyond short-term dependencies, which are usually the ones that a standard RNN model can capture due to the vanishing gradient problem. In the clockwork RNN, connections from hidden layers with slower time periods to ones with faster ones let this architecture capture relationships in longer time scales.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.5\textwidth]{img/rnn_chorale.png}
\end{center}
\caption{Chord progression generated by an RNN after training. The harmony is incoherent, and seems to move pseudo randomly. There is no clear key and the style and rules of music theory are not followed.}\label{fig:rnn_chorale}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.5\textwidth]{img/cwRNN_chorale.png}
\end{center}
\caption{Chord progression generated by the cwRNN after training. The harmony is sophisticated, and follows the rules of music composition and style from the period of the data on which the model was trained.}\label{fig:cwRNN_chorale}
\end{figure}

Finally, we made the cwRNN generate music after it was trained. Figure \ref{fig:cwRNN_chorale} contains the output for this experiment. The chorale reflects the style of the data on which it was trained. Considering it was trained on the augmented data set, it makes sense that this excerpt modulates among different keys and makes use of sophisticated harmony. These features were not observed when a chorale was generated by the ordinary RNN trained on the same data-set, as shown in Figure \ref{fig:rnn_chorale}


\section{Conclusion}
\label{sec:conclusion}

We have seen that recurrent networks, including LSTMs and clockwork RNNs, are suitable for modeling time-sequences of musical data. The language-model approach, in which we use instantaneous prediction error as a proxy for a human listener's surprise, appears to bear fruit, pending further testing and refinement. The clockwork RNN appears to perform best at musical tasks, in resistance to overfit and the generation of musically sophisticated harmonic progressions. This makes sense, as a hallmark of music is the presence of elements and motives varying over different time scales simultaneously.

With our initial reults, as well as the newly available quantitative measures of human listener surprise \cite{Roman2016musicsyntactic}, ideas for future work are manifold and exciting. In addition to prediction error, we would like to look more closely at hidden-layer activations per time step, as they may also yield information relevant to surprise levels.

We hope to continue working on the cwRNN, since it has a musically desirable architecture and generated such interesting music. Beat and rhythm are an emergent property of these models, perhaps especially with the cwRNN; we would like to explore this further, \textit{e.g.} by tuning the cwRNN time scales and/or concatenating explicit rhythmic information to the chord representation.

In the near term, we may scrutinize the embedding matrices more closely to see if there is musically significant local structure. For example, similar to analogies in NLP, perhaps the vector representations of dominant minus tonic are roughly equal. That is, $G7\ -\ C\ maj\ \simeq\ A7\ -\ D\ maj$. That and other analogies may help us confirm that the learned embeddings are musically meaningful. Additionally, we may refine the embedding matrix by initializing not randomly, but according to a heuristic such as circle of fifths relationships.


\subsubsection*{Acknowledgments}

We benefited immensely from the advice and assistance of Professors Jonathan Berger and Craig Sapp, of CCRMA and CCARH, respectively, within Stanford University's Department of Music.

% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{ieeetr}
\bibliography{cs224d-proj.bib}

\end{document}

