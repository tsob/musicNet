\documentclass{article} % For LaTeX2e
\usepackage{nips13submit_e,times}
\usepackage{hyperref}
\usepackage{url}
\usepackage{graphicx}
\usepackage{fancyvrb}
\usepackage{tikz}
%\documentstyle[nips13submit_09,times,art10]{article} % For LaTeX 2.09


\title{	A Recurrent Neural Network for\\
	Musical Structure Processing and Expectation\\
	\smallskip
	\small{CS 224D Project Milestone}}


\author{
Ir\'{a}n~Rom\'{a}n\\
CCRMA\\
Stanford University\\
Stanford, CA 94305 \\
\texttt{iran@stanford.edu} \\
\And
Tim~O'Brien\\
CCRMA\\
Stanford University\\
Stanford, CA 94305 \\
\texttt{tsobrien@stanford.edu} \\
}

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{abstract}
%The abstract must be limited to one paragraph.
%\textit{Your project milestone report should be between 2 - 3 pages using the provided template.}
%\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Music is a universal element of the human condition.
Because of its organized nature, all humans exposed to music implicitly learn the hierarchical dependencies connecting events in music.
Research in neuroscience shows that all humans are able to identify events that violate common structures in music.
However, there is a lack of models explaining how sequences of musical events are computed in the human brain.
Our goal is to building a computational model to quantitatively analyze unexpected events in music that elicit a sense of musical novelty. A recurrent neural network (RNN) can carry out such task. RNNs can develop expectations in musical sequences, relying on information about recent events and the structural dependencies in musical sequences. After training, such model could recognize unexpected events in a musical sequence in a manner similar to how humans do it.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Problem statement}
During the last 16 years, research in cognitive neuroscience has found electrophysiologicalm and behavioral evidence indicating that humans process musical structures both pre-attentively (within 200 ms of musical event onset) and attentively (after 300 ms of musical event onset). However, there is a lack of computational models explaining how musical structure processing takes place in the brain. Cognitive neuroscience provides us with evidence about how humans process sequences of events through time \cite{lalitte2006music}, how sensory and cognitive priming lead to the construction of mental representations of musical events \cite{bigand2003sensory}, and how the background of an individual affects expectation when listening a sequence of musical events \cite{schubert2006effect}.
Relying on this evidence and our knowledge about neural network models for Natural Language Processing, we develop a Recurrent Neural Network (RNN) model able to carry out the computational task of musical sequence processing. We have selected Johann Sebastian Bach's chorales (371 pieces in four voices: bass, tenor, alto, and soprano) as training corpus for our model. These chorales contain musical features that most individuals in the western world encounter throughout life when listening western music. Thus, our model structure and training set will emulate the background and ability of human subjects to process musical sequences, while our model's perplexity will serve as an indicator of surprise that human studies have found when presenting subjects with unexpected musical events.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Related work} 
Rabovsky \cite{Rabovsky2014} revisited the work by McClelland \textit{et al.} \cite{Mcclelland1989} on computational modelling of language cognition. McClelland's model, as well as other models by Frank \cite{Frank2015} for semantic processing, present large activations in a hidden layer when presented with semantic violations. These semantic violations are also known to elicit specific brain activity in humans. Even though our interests are more aligned with syntax (structure) processing than semantics, considering the structural similarities between music and language, we believe we can build a model trained on musical structures that presents similar activations when stimulated with structural violations of musical flow. Our work also takes the work by Berger and Gang \cite{Berger2010, Gang1996, Gang1998, Gang1999} as a foundation. 

\subsection{Data} %should Bach and the chorales be specifically mentioned here?
Data fed to our model is in symbolic format, derived from written scores. Our primary source is KernScores \cite{KernScores}\footnote{\url{http://kern.ccarh.org/}}, an online repository of musical scores containing over 108,703 files. Starting with the notated score allows us to focus on salient aspects of music perception, such as melody, harmony and motives. We focus on Western music from the classical period, in part because of the free availability of a large number of notated scores.

\subsection{Expected results}

We expect being able to train an RNN on a corpus of music that consists of sequences of chords. Our data is analogous in structure to language, since chords are discrete events, similarly to words, and sequences of chords form musical phrases, similar to how words in language form sentences. Thus, the dependencies between simultaneous chords in our dataset will be captured by the weights in the hidden modules of our RNN. During training, we expect to see a cost function that shows a decay as training epochs unfold. Depending on the size of our data-set and hyperparameters used, we also expect to be able to overfit our model as a sanity check. Finally, after training, we expect to be able to have our RNN generate novel musical phrases, which should turn out to be similar to those with which the model was trained.

\subsection{Evaluation Plan}

Our ultimate goals require a two-step evaluation. First, a model is trained on our corpus by implementing an ordinary train, validation, and test segmentation of input data, and fitting the model to it. Next, we will present our trained model with test signals which were developed by a member in our team, Ir\'{a}n Rom\'{a}n, for neuroscience research. Our trained model will show levels of surprise (via instantaneous model prediction error) similar to the ones found by Rom\'{a}n's experiments when humans are presented with structure violations in music. This human data will serve as a reference about the effect of musical structure violations in music processing. The prediction error in our model will be compared to these human-derived signals, allowing us to provide quantitative evidence for our thesis.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Technical Approach and Models}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Methodology/Algorithm}
Efforts to model cognitive processes unfolding through time have benefited from the use of recurrent networks. Using our data, 
% we will replicating results from Frank \cite{Frank2010} using a simple recurrent network. Crucially, following Berger \cite{Berger2010} and Gang \cite{Gang1996, Gang1998, Gang1999}, we hope to show that prediction error in these trained networks is a proxy for surprisal in the flow of musical events. Next, 
we will develop an RNN able to differentially process short-term and long-term dependencies in music. The Clockwork RNN \cite{Koutnik2014} is a novel architecture with hidden layers partitioned into modules of different sizes, able to capture the long- and short-term hierarchical features of sequences. Further, we could train this RNN on music from different time periods or, composers.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Intermediate/Preliminary Experiments \& Results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Hardware used: NVIDIA GeForce GTX 750, 1.2935 GHz.\\
Software: Fedora Linux (the Planet CCRMA distribution); Tensorflow 0.8.0; cuDNN 4, CUDA 7.5.18.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\textwidth]{img/bach-chorale-example.png}
\end{center}
\caption{The beginning of an example Bach chorale.}\label{fig:bach_chorale_score_snippet}
\end{figure}


\renewcommand{\theFancyVerbLine}{%
 \textcolor{blue}{\tiny
  t=\arabic{FancyVerbLine}}}
\begin{figure}[htb]
\begin{center}
\begin{LVerbatim}[label=Bach chorale example, numbers=left, numbersep=2pt, fontsize=\tiny, fontfamily=courier, frame=single, commandchars=\\\{\}]
\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0, 0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0, 0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0, \textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}}, 0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}}, 0,0,\textcolor{red}{\textbf{1}},0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0, 0,0,0,0,0,0,0,\textcolor{red}{\textbf{1}},0,0,0,0
\end{LVerbatim}
\end{center}
\caption{The same Bach chorale snippet as in Fig.~\ref{fig:bach_chorale_score_snippet}, encoded as concatenated length-12 row vectors for each time step, corresponding to the duration of an eighth note. The groups of 12 numbers represent, from left to right, the bass, tenor, alto, and soprano.}
\end{figure}



\begin{itemize}
    \item Bach chorales. To develop our model, we selected a corpus of music containing musical features that are common in western music. Johann Sebastian Bach, an emblematic composer from the 18th century, wrote pieces that set the stylistic foundations for composers of classical music. Among Bach's oeuvre, his chorales (371 pieces in four voices: bass, tenor, alto, and soprano) are an influential body of music sharing multiple features with western music from other composers and other eras.
    \item Input data parsing. Bach chorales consist of four different but simultaneously sounding voices. These voices coexist in separate pitch ranges, with the lowest range used by the bass, the voice in the range above it being the tenor, followed by the alto, and the highest voice range corresponding to the soprano. For en example of this musical texture, see the fragment in figure \ref{fig:bach_chorale_score_snippet}. In western music there are 12 different pitch classes within an octave. We parsed Bach chorales by concatenating four octaves (one for each voice) in a 48-dimensional vector (12 dimensions for the sets that each of the four voices could ocuppy). Thus, a 48-dimensional row vector corresponds to the sonority produced by the four simultaneously sounding voices at a point in time. Since rhythm in music can make these chords sound repeatedly for an extended period of time, repeated row vectors indicate a chord lasting longer than the our shortest unit of musical subdivision, which we decided to be an eight note. Figure \ref{fig:chord_incidence_hist} shows the number of times chords were repeated in our original and augmented dataset (described below). Thus, in our model, a "word" is each of these word vectors, and a "sentence" is a sequence of these row vectors that make-up an entire chorale.
    \item For this milestone, we trained a previously existing Long Short-Term Memory (LSTM) model using our dataset on tensorflow. This previously existing model was the PTB model from \cite{zaremba2014recurrent}, which is an LSTM for word prediction on the Penn Tree Bank (hence the name PTB) dataset. This model turned out to be ideal for our purposes, as our main goal for this milestone was to develop a model able to predict the next chord in a sequence of musical chords, and show this prediction in its measured perplexity on training, validation, and test sets. 
\end{itemize}

\begin{figure}[htb]
\begin{center}
\framebox[0.82\textwidth]{\includegraphics[width=0.8\textwidth]{img/chord_incidence_hist.png}}
\end{center}
\label{fig:chord_incidence_hist}
\caption{Number of times chords appeared in our dataset. Our vocabulary size in the original data set is around 3000, and in the augmented dataset is around 50,000. The top row of this figure shows the incidence for chords in the original dataset, while the bottom row of the figure shows the incidence for chords in the augmented dataset.}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NON-AUGMENTED RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb]
\begin{center}
\framebox[0.82\textwidth]{\includegraphics[width=0.8\textwidth]{img/cost.png}}
\end{center}
\caption{Values of the cost function during training on the original dataset.}
\end{figure}

\begin{figure}[htb]
\begin{center}
\framebox[0.82\textwidth]{\includegraphics[width=0.8\textwidth]{img/train_and_val_per_epoch_nonaugmented.pdf}}
\end{center}
\caption{Values of the perplexity, per training epoch, for the training and testing set (from the original dataset).}
\end{figure}

\begin{table}[htb]
\caption{The final values of perplexity (after epoch 39) for the (non-augmented) Bach chorale dataset.}
\label{tab:final-perplexity-nonaugmented}
\begin{center}
\begin{tabular}{ll}
\multicolumn{1}{c}{\bf Data fold}  &\multicolumn{1}{c}{\bf Perplexity}
\\ \hline \\
Train       & 18.925 \\
Validation  & 64.329 \\
Test        & 51.780 \\
\end{tabular}
\end{center}
\end{table}


\begin{table}[t]
\caption{Values of hyperparameters used with the (non-augmented) Bach chorale dataset.}
\label{tab:final-hyperparams-nonaugmented}
\begin{center}
\begin{BVerbatim}
init_scale    =     0.05
learning_rate =     1.0
max_grad_norm =     5
num_layers    =     2
num_steps     =    35
hidden_size   =   400
max_epoch     =     6
max_max_epoch =    39
keep_prob     =     0.5
lr_decay      =     0.8
batch_size    =    20
vocab_size    = 10000
\end{BVerbatim}
\end{center}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AUGMENTED RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb]
\begin{center}
\framebox[0.82\textwidth]{\includegraphics[width=0.8\textwidth]{img/cost_augmented.png}}
\end{center}
\caption{Values of the cost function during training over the augmented dataset.}
\end{figure}

\begin{figure}[htb]
\begin{center}
\framebox[0.82\textwidth]{\includegraphics[width=0.8\textwidth]{img/train_and_val_per_epoch.pdf}}
\end{center}
\caption{Values of the perplexity, per training epoch, for the training and testing set (from the augmented dataset).}
\end{figure}


\begin{table}[t]
\caption{The final values of perplexity (after epoch 39) for the augmented Bach chorale dataset.}
\label{tab:final-perplexity-augmented}
\begin{center}
\begin{tabular}{ll}
\multicolumn{1}{c}{\bf Data fold}  &\multicolumn{1}{c}{\bf Perplexity}
\\ \hline \\
Train       & 27.065 \\
Validation  & 32.570 \\
Test        & 34.024 \\
\end{tabular}
\end{center}
\end{table}


\begin{table}[t]
\caption{Values of hyperparameters used with the augmented Bach chorale dataset.}
\label{tab:final-hyperparams-augmented}
\begin{center}
\begin{BVerbatim}
init_scale    =     0.05                                                             
learning_rate =     1.0                                                           
max_grad_norm =     5                                                             
num_layers    =     2                                                                
num_steps     =    16
hidden_size   =   200
max_epoch     =     6                                                                 
max_max_epoch =    39                                                            
keep_prob     =     0.5                                                               
lr_decay      =     0.8                                                                
batch_size    =    12
vocab_size    = 50000
\end{BVerbatim}
\end{center}
\end{table}


\begin{itemize}
    \item Data augmentation via transposition. In order to expand our dataset, we used a standard method from music theory to transform our musical material by "transpostion", which changes the reference pitch that defines the "tonic" of the piece. In the original dataset, all chorales shared the same unique key, but any of the twelve pitches in the set can serve as the tonic. Thus, our data augmentation transposed the dataset to different keys, so that all chorales were presented to our RNN model in all 12 possible keys.
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{ieeetr}
\bibliography{cs224d-proj.bib}

\end{document}
