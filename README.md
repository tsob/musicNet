# Music Net

Irán Román, <iran@stanford.edu>, Tim O'Brien, <tsobrien@stanford.edu>

This is the working repository for recurrent neural nets which learn music
theory and structure. For more details, see our [project report](reports/final/final.pdf)

## Prerequisites

1. For data sets, and data parsing, you'll want to install Humdrum. Hopefully [this link](http://www.humdrum.org/install/github/) can walk you through that.
2. We use Python 2.7 with various packages via PIP. See [`src/requirements.txt`](src/requirements.txt) for necessary packages.
    * Specifically for TensorFlow, you can follow [these installation instructions](https://www.tensorflow.org/versions/r0.8/get_started/os_setup.html#pip-installation).