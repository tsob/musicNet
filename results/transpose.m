% script to transpose a progression from a key ~= Cmajor to Cmajor


fid = fopen('gen_prog_cwrnn.txt');

tline = fgetl(fid);
while ischar(tline)
    disp(tline)
    tline = fgetl(fid);
end

fclose(fid);
