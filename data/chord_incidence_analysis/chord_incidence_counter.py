"""Utilities for parsing text files."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import os

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

# to count original files
train_path = os.path.join("../bach-chorales-20160512/", "chr.train.txt")

with tf.gfile.GFile(train_path, "r") as f:
  data = f.read().replace("\n", "<eos>").split()

hist_dict = {'11-50': 0, '51-100': 0, '101-200':0, '>200':0}
hist_dict2 = {'1-10': 0}
counter = collections.Counter(data)

for key in counter:

    if counter[key] > 0 and counter[key] <= 10:
        hist_dict2['1-10'] = hist_dict2['1-10'] + 1    

    if counter[key] > 10 and counter[key] <= 50:
        hist_dict['11-50'] = hist_dict['11-50'] + 1 
    if counter[key] >= 51 and counter[key] <= 100:
        hist_dict['51-100'] = hist_dict['51-100'] + 1 
    elif counter[key] >= 101 and counter[key] <= 200:
        hist_dict['101-200'] = hist_dict['101-200'] +1
    elif counter[key] > 201:
        hist_dict['>200'] = hist_dict['>200'] +1 

train_path = os.path.join("../chor_data_augmented/", "ptb.train.txt")

with tf.gfile.GFile(train_path, "r") as f:
  data = f.read().replace("\n", "<eos>").split()

hist_dict1 = {'11-50': 0, '51-100': 0, '101-200':0, '>200':0}
hist_dict12 = {'1-10': 0}
counter = collections.Counter(data)

for key in counter:

    if counter[key] > 0 and counter[key] <= 10:
        hist_dict12['1-10'] = hist_dict12['1-10'] + 1    

    if counter[key] > 10 and counter[key] <= 50:
        hist_dict1['11-50'] = hist_dict1['11-50'] + 1 
    if counter[key] >= 51 and counter[key] <= 100:
        hist_dict1['51-100'] = hist_dict1['51-100'] + 1 
    elif counter[key] >= 101 and counter[key] <= 200:
        hist_dict1['101-200'] = hist_dict1['101-200'] +1
    elif counter[key] > 201:
        hist_dict1['>200'] = hist_dict1['>200'] +1 

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

gs = gridspec.GridSpec(11, 5)

plt.subplot(gs[0:4,0])
plt.title('Original Dataset', size = 16)
plt.bar(range(len(hist_dict2)), hist_dict2.values(), align='center')
plt.xticks(range(len(hist_dict2)), hist_dict2.keys())
plt.ylabel('Number of chords with such incidence')
plt.xlabel('Chord incidence in dataset')

plt.subplot(gs[0:4,2:5])
plt.bar(range(len(hist_dict)), hist_dict.values(), align='center')
plt.xticks(range(len(hist_dict)), hist_dict.keys())
plt.ylabel('Number of chords with such incidence')
plt.xlabel('Chord incidence in dataset')

plt.subplot(gs[6:,0])
plt.title('Augmented Dataset', size = 16)
plt.bar(range(len(hist_dict12)), hist_dict12.values(), align='center')
plt.xticks(range(len(hist_dict12)), hist_dict12.keys())
plt.ylabel('Number of chords with such incidence')
plt.xlabel('Chord incidence in dataset')

plt.subplot(gs[6:,2:5])
plt.bar(range(len(hist_dict1)), hist_dict1.values(), align='center')
plt.xticks(range(len(hist_dict1)), hist_dict1.keys())
plt.ylabel('Number of chords with such incidence')
plt.xlabel('Chord incidence in dataset')

plt.show()
