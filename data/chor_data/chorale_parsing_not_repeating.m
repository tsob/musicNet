% file to parse back chorale data into a word representation
% without repeating same chords

num_chorales = 371;
ommited_chorale = 150;

train = fopen('../bach-chorales-20160512/chr.train.txt', 'wt');
val = fopen('../bach-chorales-20160512/chr.val.txt', 'wt');
test = fopen('../bach-chorales-20160512/chr.test.txt', 'wt');

for i=1:num_chorales
    if i == ommited_chorale
        continue;
    end
    
    chr = csvread(sprintf('../bach-chorales-20160512/chor%03d.txt', i));
    
    word_copy = '[000000000000000000000000000000000000000000000000]';
    sentence = [];
    for j = 1:size(chr(:,1))
        word = mat2str(chr(j,:));
        word(ismember(word,' ,.:;!')) = [];
        if ~strcmp(word, word_copy);
            sentence = strcat(sentence, [' ' word]);
        end
        word_copy = word;
    end
    
    sentence = strcat(sentence, '\n');
    
    % split the data into train, validation, and test sets
    random_num = randi(10,1);
    if (random_num >= 1) && (random_num <= 8)
        fprintf(train, sentence);
    elseif random_num == 9
        fprintf(val, sentence);
    elseif random_num == 10
        fprintf(test, sentence);
    end
        
end

 fclose(train);
 fclose(val);
 fclose(test);
 
 
 
 