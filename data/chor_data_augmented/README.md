# Data Augmentation on Bach Chorale Data

This directory, `./data/chor_data_augmented/`, contains the same data as
`./data/chor_data_augmented/`, but augment by 12x due to transposing each
chorale to all semitone keys. (Modes are, of course, keps the same -- this is
just a simple transposition of parts.)
