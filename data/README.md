# Bach chorales

Parsed data from the Bach chorales is located in `./data/bach-chorales-20160512`. Documentation on this data is below, provided by the amazing [Professor Craig Sapp](https://ccrma.stanford.edu/~craig/).

Here is sample data:

```
1,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,1,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0, 1,0,0,0,0,0,0,0,0,0,0,0
1,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,1,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0, 1,0,0,0,0,0,0,0,0,0,0,0
1,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,1,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0, 1,0,0,0,0,0,0,0,0,0,0,0
1,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,1,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0, 1,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,1,0,0, 0,0,0,0,0,1,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,1,0,0, 1,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,1,0,0, 0,0,0,0,1,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,1,0,0, 1,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,1, 0,0,1,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,1, 0,0,1,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0
```

Each row is a time instant in the score and represents an 8th note duration.  The data is separated by voice in the score, where the first 12 numbers on a line are for the lowest part (bass for the chorales), and the going up the system as you go to the right (–π/2 rotation of the score to get to the data orientation).  A space is added between each part for readability.

The first number for each part is the tonic of the chorale (all chorales are transposed to C).  Then C#/D-flat, D, D#/E-flat, E, F, etc.

The above sample is for this measure:

![Bach chorale example](https://cm-gitlab.stanford.edu/tsob/musicNet/raw/master/data/img/bach-chorale-example.png "Bach chorale example")

But of course transposed to C, so G in the bass will be C when transposed, and shows up in the music in the first column of the data. The next note in the bass is a G (transposed to C), so the first two rows of data also contain 1s for that note.

Everything seems to be working well, based on basic testing of the conversion.  Here is a sample Humdrum score:

```
**kern **kern **kern **kern
4c     4c#    4d     4d#
16c    16r    4B-    16r
16c#   16r    .      16r
16d    16r    .      8B
16e-   16r    .      .
*-     *-     *-     *-
```

And here is the converted data:

```
1,0,0,0,0,0,0,0,0,0,0,0, 0,1,0,0,0,0,0,0,0,0,0,0, 0,0,1,0,0,0,0,0,0,0,0,0, 0,0,0,1,0,0,0,0,0,0,0,0
1,0,0,0,0,0,0,0,0,0,0,0, 0,1,0,0,0,0,0,0,0,0,0,0, 0,0,1,0,0,0,0,0,0,0,0,0, 0,0,0,1,0,0,0,0,0,0,0,0
1,1,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,1,0, 0,0,0,0,0,0,0,0,0,0,0,0
0,0,1,1,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,1,0, 0,0,0,0,0,0,0,0,0,0,0,1
```

which all looks good.  Notice the 16th notes in the first column of the Humdrum score which are showing up as double hits on each 8th note duration line:

```
1,1,0,0,0,0,0,0,0,0,0,0,
0,0,1,1,0,0,0,0,0,0,0,0
```

The first line "1,1" is for the 16c and 16c# tokens (16th note C and 16th note C#).
The second line "1,1" is for 16d and 16e- tokens (16th note D and 16th note E-flat).

## Modes

Note that these files are in *Dorian* mode (closest to minor):

    chor003 chor008 chor015 chor019 chor025 chor049 chor082 chor087 chor110
    chor113 chor119 chor122 chor133 chor155 chor162 chor166 chor168 chor171
    chor174 chor180 chor184 chor185 chor186 chor197 chor199 chor203 chor206
    chor210 chor218 chor232 chor237 chor241 chor244 chor262 chor302 chor325
    chor369

These chorales are in *Phrygian* (closest to minor):

    chor034 chor205

*Mixolydian* mode (closest to major):

    chor070 chor127 chor154 chor160 chor187 chor231 chor284 chor288 chor357

*Major* mode chorales:

    001 002 004 005 006 007 009 011 014 018 020 022 024 026 027 029 032 035
    036 038 040 042 043 044 046 050 051 052 054 058 060 061 063 064 065 067
    068 069 074 076 077 080 083 084 085 086 090 093 095 097 098 101 102 103
    106 107 108 116 117 118 121 124 125 128 131 135 136 137 139 140 141 143
    144 147 148 151 152 153 156 157 158 159 164 165 169 173 175 176 177 179
    183 188 189 192 195 200 201 202 209 211 212 216 217 222 223 224 233 234
    235 239 246 247 248 249 250 252 254 255 256 257 258 260 264 268 272 273
    274 275 276 277 278 279 280 282 289 290 291 293 296 298 299 303 305 306
    308 309 310 311 312 313 315 316 317 318 319 322 323 326 327 328 329 330
    333 334 335 337 338 341 342 343 344 347 348 350 351 353 354 355 361 362
    363 365 366 368

Minor mode chorales:

    010 012 013 016 017 021 023 028 030 031 033 037 039 041 045 047 048 053
    055 056 057 059 062 066 071 072 073 075 078 079 081 088 089 091 092 094
    096 099 100 104 105 109 111 112 114 115 120 123 126 129 130 132 134 138
    142 145 146 149 161 163 167 170 172 178 181 182 190 191 193 194 196 198
    204 207 208 213 214 215 219 220 221 225 226 227 228 229 230 236 238 240
    242 243 245 251 253 259 261 263 265 266 267 269 270 271 281 283 285 286
    287 292 294 295 297 300 301 304 307 314 320 321 324 331 332 336 339 340
    345 346 349 352 356 358 359 360 364 367 370 371

In theory, there should be 370 numbers listed above.

`chor150` is not included, as it is a 5-part chorale.

The webpage [http://kern.ccarh.org/cgi-bin/ksbrowse?l=chorales](http://kern.ccarh.org/cgi-bin/ksbrowse?l=chorales) has links to the music notation.  The "S" buttons before each work links to a PDF of the scanned score. Only a few scans are there, so the other scores can be seen by clicking on the "A" buttons which is a score generated from the Humdrum file directly.

Here is the PERL script which was used to generate the data files:

```
#!/usr/bin/perl
@files = glob "*.krn";
foreach $file (@files) {
   $base = $file;
   $base =~ s/\.krn//;
   `transpose -k c $file | rnn-input > $base.txt`;
}
```

```rnn-input``` is the program to extract the data from the Humdrum scores.

The command `transpose -k c` means to transpose the score so that C is the tonic.

# Data Augmentation by Key Transposition

The file `chorale_augmentation.py` takes the chorales in
`bach-chorales-20160512/` and transposes each part to all of the 12 semitone
scales. (Modes remain inchanged -- this is just a naive transposition). An
example of this appears below.

Take the first line of `bach-chorales-20160512/chor001.txt`:
```
1,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,1,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0, 1,0,0,0,0,0,0,0,0,0,0,0
```
becomes the following (transposed 0, 1, 2, ..., 10, and 11 semitones):

```
1,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,1,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,1,0,0,0,0, 1,0,0,0,0,0,0,0,0,0,0,0
0,1,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,1,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,1,0,0,0, 0,1,0,0,0,0,0,0,0,0,0,0
0,0,1,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,1,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,1,0,0, 0,0,1,0,0,0,0,0,0,0,0,0
...
0,0,0,0,0,0,0,0,0,0,1,0,0, 0,1,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,1,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,1,0
0,0,0,0,0,0,0,0,0,0,0,1,0, 0,0,1,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,1,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,1
```

which are the first lines in
`bach-chorales-20160512-transposed/chor001-00.txt`, `chor001-01.txt`, `chor001-02.txt`, ..., `chor001-10.txt`, and `chor001-11.txt`.

Further, to combine all this into a format that our simple lstm likes, you can
go to `chor_data_augmented/` and run the Matlab script, `chorale_parsing.m`.
Just make sure to delete old versions of `ptb.*.txt` in that directory before
regenerating them.
