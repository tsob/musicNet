#!/usr/bin/env python
"""
@name: chorale_augmentation.py
@desc: For each Bach chorale file, generate 12 files for each key.
@auth: Tim O'Brien
@date: May 15, 2016
"""

import numpy as np
import os
import glob
import csv

def main():
    # Define where our data lives
    data_dir = os.path.abspath(os.path.join(
        '.', 'bach-chorales-20160512'
        ))
    output_dir = os.path.abspath(os.path.join(
        '.', 'bach-chorales-20160512-transposed'
        ))
    data_filenames = [os.path.join(data_dir, os.path.basename(f)) \
                      for f in glob.glob(os.path.join(data_dir, '*.txt'))]

    num_files = len(data_filenames)

    octave = np.arange(12)
    idx = np.zeros((12,12), dtype=int)
    for i in octave:
        idx[i] = np.roll(octave, i)
    idx = np.hstack((idx, idx+12, idx+24, idx+36))

    for chor_filename in data_filenames:
        with open(chor_filename, "rb") as f_in:
            reader = csv.reader(f_in)
            for line in reader:
                line = np.asarray(line)
                print line.shape
                return
                for i in octave:
                    out_line = line[idx[i]].tolist()
                    chor_out_filename = os.path.join(
                            output_dir,
                            os.path.splitext(os.path.basename(chor_filename))[0] +\
                            '-{0:02d}'.format(i) +\
                            os.path.splitext(os.path.basename(chor_filename))[1]
                            )
                    with open(chor_out_filename, "ab") as f_out:
                        writer = csv.writer(f_out)
                        writer.writerow(out_line)

if __name__=="__main__":
    main()
