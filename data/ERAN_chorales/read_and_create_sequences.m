%% read_and_create_chords.m
%% 30 Jan 2002
%% last edit 07 May 2002

%% by T.A.FUJIOKA
%% Modified by Iran Roman %% 24 May 2016

%% definition of chord of 4 tones
%% each tone is 1 wav file.
%filename_head is string
%def_filename = input('Please input the filename of melody definition
%(def_foo.txt):','s');
%def_filename= 'def_chord1.txt';
% def_filename= 'def_control_chord_progressions.txt';
% def_filename= 'def_local_and_global_chord_progressions.txt';
def_filename= 'def_local_only_chord_progressions.txt';

%% def_task.txt contains all definition of chords (4-note).

% extract information from the text file
[root_name, chord_type, duration, voicing, tone1, tone2, tone3, tone4] = textread(def_filename,'%s %s %d %s %s %s %s %s');

% identify note names and their pitch classes
note_names = ['C', 'D', 'E', 'F', 'G', 'A', 'B'];
note_idx = [1, 3, 5, 6, 8, 10, 12];

% create array with all chords
all_tone = [tone1 tone2 tone3 tone4];

% template voice to fill in
empty_voice = '000000000000';

% open file to write on
% data = fopen('control.txt', 'wt');
% data = fopen('local_and_global.txt', 'wt');
data = fopen('local_only.txt', 'wt');

for j = 1 : (length(all_tone) / 9)
    
    chorale = [];
    
    current_key = root_name(1 + (9 * (j -1))); 
    
    check_end_of_chorale = 0;
    i = 1 + ((j - 1) * 9);
    while check_end_of_chorale < 9;
        Bass = all_tone(i,1);
        Tenor = all_tone(i,2);
        Alto = all_tone(i,3);
        Soprano = all_tone(i,4);
                      
        Bass_class = note_idx(find(note_names == Bass{1, 1}(1), 1));
        if Bass{1, 1}(3) == 'S';
            Bass_class = Bass_class + 1;
        end
        Tenor_class = note_idx(find(note_names == Tenor{1, 1}(1), 1));
        if Tenor{1, 1}(3) == 'S';
            Tenor_class = Tenor_class + 1;
        end
        Alto_class = note_idx(find(note_names == Alto{1, 1}(1), 1));
        if Alto{1, 1}(3) == 'S';
            Alto_class = Alto_class + 1;
        end
        Soprano_class = note_idx(find(note_names == Soprano{1, 1}(1), 1));
        if Soprano{1, 1}(3) == 'S';
            Soprano_class = Soprano_class + 1;
        end
        
        if length(current_key{1, 1}) > 1
        key_shift = note_idx(find(note_names == current_key{1, 1}(1))) - 2;
        else
        key_shift = note_idx(find(note_names == current_key{1, 1}(1))) - 1;
        end
        
        ev_copy = empty_voice;
        if mod((Bass_class - key_shift), 12) ~= 0 
        ev_copy(mod((Bass_class - key_shift), 12)) = '1';
        else
        ev_copy(12) = '1';
        end
        Bass_voice = ev_copy;
        
        ev_copy = empty_voice;
        if mod((Tenor_class - key_shift), 12) ~= 0
        ev_copy(mod((Tenor_class - key_shift), 12)) = '1';
        else
        ev_copy(12) = '1';
        end
        Tenor_voice = ev_copy;
        
        ev_copy = empty_voice;
        if mod((Alto_class - key_shift), 12) ~= 0 
        ev_copy(mod((Alto_class - key_shift), 12)) = '1';
        else
        ev_copy(12) = '1';
        end
        Alto_voice = ev_copy;
        
        ev_copy = empty_voice;
        if mod((Soprano_class - key_shift), 12) ~= 0 
        ev_copy(mod((Soprano_class - key_shift), 12)) = '1';
        else
        ev_copy(12) = '1';
        end
        Soprano_voice = ev_copy;
        
        chord = ['[' Bass_voice Tenor_voice Alto_voice Soprano_voice ']'];
        
        for repeat = 1 : ceil((duration(i) / 250))
        
        chorale = [chorale ' ' chord];
        
        end
        
        i = i +1;
        
        check_end_of_chorale =  check_end_of_chorale + 1;
    end
    fprintf(data, [chorale '\n']);
end

fclose(data);