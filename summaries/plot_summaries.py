#!/usr/env/python
"""
Plot epoch-level summaries of training run
"""

import numpy as np
import matplotlib.pyplot as plt
import os

def main():
    data_dir = os.path.abspath('.')
    train_filename = os.path.join(
            data_dir, 'chor_nonaugmented_train_perp_per_epoch.txt')
    val_filename = os.path.join(
            data_dir, 'chor_nonaugmented_valid_perp_per_epoch.txt')
    # lr_filename = os.path.join(
            # data_dir, 'chor_augmented_learning_rate_per_epoch.txt')
    train_perp = np.genfromtxt(train_filename)
    valid_perp = np.genfromtxt(val_filename)

    plt.figure()
    plt.plot(train_perp, linewidth=2.0, label='Training')
    plt.plot(valid_perp, linewidth=2.0, label='Validation')
    plt.grid(True)
    plt.xlabel('Epoch')
    plt.ylabel('Perplexity')
    plt.legend()
    plt.show()


if __name__=="__main__":
    main()
